\# xpub-converter-api

This Node.js application provides an API for converting xpub keys to different formats.

## Prerequisites

Before running the application, make sure you have the following installed:

- [Docker](https://www.docker.com/)

## Getting Started

1. **Option 1: Build the Docker image yourself:**

    ```bash
    git clone https://gitlab.com/masterdam79/xpub-converter.git
    cd xpub-converter
    docker build -t masterdam79/xpub-converter-api .
    docker run -p 3000:3000 masterdam79/xpub-converter-api
    ```

2. **Option 2: Run the Docker container using a pre-built image:**

    ```bash
    docker pull masterdam79/xpub-converter-api
    docker run -p 3000:3000 masterdam79/xpub-converter-api
    ```

The API will be accessible at http://localhost:3000.

## API Usage

### Convert xpub

Send a POST request to `/convert` with the following JSON payload:

```json
{
  "xpub": "your_xpub_here",
  "format": "desired_format"
}
```

- `xpub`: The original xpub key.
- `format`: The desired format for conversion (e.g., "zpub").

Example using cURL:

```bash
curl -X POST -H "Content-Type: application/json" -d '{"xpub": "your_xpub_here", "format": "zpub"}' http://localhost:3000/convert
```

## Docker Deployment

If you prefer to deploy the application using Docker, follow these steps:

1. Build the Docker image:

    ```bash
    docker build -t masterdam79/xpub-converter-api .
    ```

2. Push the Docker image to Docker Hub:

    ```bash
    docker login
    docker push masterdam79/xpub-converter-api
    ```

3. Pull and run the Docker container on your production server:

    ```bash
    docker run -p 3000:3000 masterdam79/xpub-converter-api
    ```

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
