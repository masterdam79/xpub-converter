const express = require('express');
const xpubConverter = require('xpub-converter');

const app = express();
const port = 3000; // Adjust the port as needed

app.use(express.json());

app.post('/convert', (req, res) => {
  const { xpub, format } = req.body;

  if (!xpub || !format) {
    return res.status(400).json({ error: 'xpub and format are required in the request body' });
  }

  try {
    const convertedKey = xpubConverter(xpub, format);
    res.json({ originalXpub: xpub, convertedKey, format });
  } catch (error) {
    res.status(500).json({ error: 'An error occurred during conversion' });
  }
});

// New endpoint for usage information
app.get('/', (req, res) => {
  res.send('Welcome to the xpub-converter API! Use POST request to /convert with JSON body { "xpub": "<xpub_key>", "format": "<desired_format>" }');
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
